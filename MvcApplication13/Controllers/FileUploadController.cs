﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication13.Controllers
{
    public class FileUploadController : Controller
    {
        //
        // GET: /FileUpload/

        UploadEntities1 db = new UploadEntities1();

        //
        // GET: /FileUpload/

        public ActionResult List()
        {
            return View(db.Tables.ToList());
        }
        public ActionResult submit()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult submit(Table t, HttpPostedFileBase file)
        {

            if (file != null)
            {
                int MaxContentLength = 1024 * 1024 * 30; //30 MB
                string[] AllowedFileExtensions = new string[] { ".jpg", ".gif", ".png", ".pdf", ".html", ".htm", ".wmv", ".mp4", ".mp3", ".wav" };

                if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
                {
                    // ModelState.AddModelError("File", "Please file of type: " + string.Join(", ", AllowedFileExtensions));
                    ViewBag.Message = "Please file of type: " + string.Join(", ", AllowedFileExtensions);
                }

                else if (file.ContentLength > MaxContentLength)
                {
                    return Content("Your file is too large, maximum allowed size is: " + MaxContentLength + " MB");
                }
                else
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/Content/Upload"), fileName);


                    file.SaveAs(path);
                    ModelState.Clear();
                    ViewBag.Message = "File uploaded successfully";
                    string ds = file.FileName.Substring(file.FileName.Length - 3);
                    string p = string.Empty;
                    p = Server.MapPath("~/Content/Upload/");
                    file.SaveAs(p + file.FileName);

                    using(db)
                    {
                        t.file = file.FileName;
                        db.Tables.Add(t);
                        db.SaveChanges();
                    }
                }

            }
            else
            {
                return Content("Please Upload Your file");
            }
            return RedirectToAction("List");
        }

        public ActionResult Details(int id)
        {
            return View(db.Tables.Find(id));
        }

        public ActionResult Edit(int id)
        {
            return View(db.Tables.Find(id));
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Edit(Table t, HttpPostedFileBase file)
        {
            if (file != null)
            {
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Content/Upload"), fileName);


                file.SaveAs(path);
                ModelState.Clear();
                ViewBag.Message = "File updated successfully";
                string sd = file.FileName.Substring(file.FileName.Length - 3);
                string p = string.Empty;
                p = Server.MapPath("~/Content/Upload");
                file.SaveAs(p + file.FileName);

                using (db)
                {
                    t.file = file.FileName;
                    db.Entry(t).State = EntityState.Modified;
                    db.SaveChanges();
                }

            }
            return RedirectToAction("List");
        }
        public ActionResult Delete(int id)
        {
            return View(db.Tables.Find(id));

        }
        [HttpPost, ActionName("Delete")]
        public ActionResult delete_conf(int id)
        {
            Table t = db.Tables.Find(id);
            db.Tables.Remove(t);
            db.SaveChanges();
            return RedirectToAction("List");
        }

    }
}
